package pl.piotrkulma.euler.solutions;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Problem5Test {
    @Test
    public void isPalindromeTest() {
        Problem5 problem5 = new Problem5();
        assertEquals(problem5.isEvenlyDivisable(2520 , 10), true);

        assertEquals(problem5.isEvenlyDivisable(15 , 10), false);
    }
}
