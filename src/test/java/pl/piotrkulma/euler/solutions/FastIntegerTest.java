package pl.piotrkulma.euler.solutions;

import org.junit.Test;
import pl.piotrkulma.euler.solutions.utils.FastInteger;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class FastIntegerTest {

    @Test
    public void compareToTest() {
        assertEquals(Integer.compare(0, 0), compareTo("0", "0"));
        assertEquals(Integer.compare(1, 1), compareTo("1", "1"));
        assertEquals(Integer.compare(5, 5), compareTo("5", "5"));
        assertEquals(Integer.compare(10, 10), compareTo("10", "10"));
        assertEquals(Integer.compare(0, 10), compareTo("0", "10"));
        assertEquals(Integer.compare(10, 0), compareTo("10", "0"));
        assertEquals(Integer.compare(999, 99), compareTo("999", "99"));
        assertEquals(Integer.compare(99, 999), compareTo("99", "999"));
        assertEquals(Integer.compare(221, 222), compareTo("221", "222"));
        assertEquals(Integer.compare(222, 221), compareTo("222", "221"));
        assertEquals(Integer.compare(122, 222), compareTo("122", "222"));
        assertEquals(Integer.compare(222, 122), compareTo("222", "122"));
        assertEquals(Integer.compare(22022, 22122), compareTo("22022", "22122"));
        assertEquals(Integer.compare(22122, 22022), compareTo("22122", "22022"));
    }

    @Test
    public void subTest() {
        assertEquals((111 - - 999), Integer.parseInt(sub("111", "-999")));
        assertEquals((-111 - - 999), Integer.parseInt(sub("-111", "-999")));
        assertEquals((-111 - 999), Integer.parseInt(sub("-111", "999")));
        assertEquals((111 - 999), Integer.parseInt(sub("111", "999")));

        assertEquals((999 - - 111), Integer.parseInt(sub("999", "-111")));
        assertEquals((-999 - - 111), Integer.parseInt(sub("-999", "-111")));
        assertEquals((-999 - 111), Integer.parseInt(sub("-999", "111")));
        assertEquals((999 - 111), Integer.parseInt(sub("999", "111")));

        assertEquals((19191919 - - 919191), Integer.parseInt(sub("19191919", "-919191")));
        assertEquals((-19191919 - - 919191), Integer.parseInt(sub("-19191919", "-919191")));
        assertEquals((-19191919 - 919191), Integer.parseInt(sub("-19191919", "919191")));
        assertEquals((19191919 - 919191), Integer.parseInt(sub("19191919", "919191")));

        assertEquals((919191 - - 19191919), Integer.parseInt(sub("919191", "-19191919")));
        assertEquals((-919191 - - 19191919), Integer.parseInt(sub("-919191", "-19191919")));
        assertEquals((-919191 - 19191919), Integer.parseInt(sub("-919191", "19191919")));
        assertEquals((919191 - 19191919), Integer.parseInt(sub("919191", "19191919")));

        assertEquals((0 - 0), Integer.parseInt(sub("0", "0")));
        assertEquals((1 - 1), Integer.parseInt(sub("1", "1")));
        assertEquals((2 - 2), Integer.parseInt(sub("2", "2")));
        assertEquals((3 - 3), Integer.parseInt(sub("3", "3")));
        assertEquals((4 - 4), Integer.parseInt(sub("4", "4")));
        assertEquals((5 - 5), Integer.parseInt(sub("5", "5")));
        assertEquals((6 - 6), Integer.parseInt(sub("6", "6")));
        assertEquals((7 - 7), Integer.parseInt(sub("7", "7")));
        assertEquals((8 - 8), Integer.parseInt(sub("8", "8")));
        assertEquals((9 - 9), Integer.parseInt(sub("9", "9")));
        assertEquals((10 - 10), Integer.parseInt(sub("10", "10")));

        assertEquals((91919191 - 191919), Integer.parseInt(sub("91919191", "191919")));
    }

    @Test
    public void multTest() {
        assertEquals((0 * 0), Integer.parseInt(mult("0", "0")));
        assertEquals((1 * 1), Integer.parseInt(mult("1", "1")));
        assertEquals((2 * 2), Integer.parseInt(mult("2", "2")));
        assertEquals((3 * 3), Integer.parseInt(mult("3", "3")));
        assertEquals((4 * 4), Integer.parseInt(mult("4", "4")));
        assertEquals((5 * 5), Integer.parseInt(mult("5", "5")));
        assertEquals((6 * 6), Integer.parseInt(mult("6", "6")));
        assertEquals((7 * 7), Integer.parseInt(mult("7", "7")));
        assertEquals((8 * 8), Integer.parseInt(mult("8", "8")));
        assertEquals((9 * 9), Integer.parseInt(mult("9", "9")));
        assertEquals((10 * 10), Integer.parseInt(mult("10", "10")));
    }

    @Test
    public void addTest() {
        assertEquals((0 + 0), Integer.parseInt(add("0", "0")));
        assertEquals((1 + 1), Integer.parseInt(add("1", "1")));
        assertEquals((2 + 2), Integer.parseInt(add("2", "2")));
        assertEquals((3 + 3), Integer.parseInt(add("3", "3")));
        assertEquals((4 + 4), Integer.parseInt(add("4", "4")));
        assertEquals((5 + 5), Integer.parseInt(add("5", "5")));
        assertEquals((6 + 6), Integer.parseInt(add("6", "6")));
        assertEquals((7 + 7), Integer.parseInt(add("7", "7")));
        assertEquals((8 + 8), Integer.parseInt(add("8", "8")));
        assertEquals((9 + 9), Integer.parseInt(add("9", "9")));
        assertEquals((10 + 10), Integer.parseInt(add("10", "10")));

        assertEquals((11 + 11), Integer.parseInt(add("11", "11")));
        assertEquals((22 + 22), Integer.parseInt(add("22", "22")));
        assertEquals((33 + 33), Integer.parseInt(add("33", "33")));
        assertEquals((44 + 44), Integer.parseInt(add("44", "44")));
        assertEquals((55 + 55), Integer.parseInt(add("55", "55")));
        assertEquals((66 + 66), Integer.parseInt(add("66", "66")));
        assertEquals((77 + 77), Integer.parseInt(add("77", "77")));
        assertEquals((88 + 88), Integer.parseInt(add("88", "88")));
        assertEquals((99 + 99), Integer.parseInt(add("99", "99")));

        assertEquals((100 + 100), Integer.parseInt(add("100", "100")));
        assertEquals((555 + 555), Integer.parseInt(add("555", "555")));
        assertEquals((999 + 999), Integer.parseInt(add("999", "999")));

        assertEquals((1 + 0), Integer.parseInt(add("1", "0")));
        assertEquals((0 + 1), Integer.parseInt(add("0", "1")));

        assertEquals((9 + 1), Integer.parseInt(add("9", "1")));
        assertEquals((1 + 9), Integer.parseInt(add("1", "9")));

        assertEquals((60 + 1), Integer.parseInt(add("60", "1")));
        assertEquals((1 + 60), Integer.parseInt(add("1", "60")));


        assertEquals((111111111 + 111111111), Integer.parseInt(add("111111111", "111111111")));

        assertEquals((911111111 + 111111111), Integer.parseInt(add("911111111", "111111111")));
        assertEquals((111111111 + 911111111), Integer.parseInt(add("111111111", "911111111")));

        assertEquals((9999999 + 999), Integer.parseInt(add("9999999", "999")));
        assertEquals((999 + 9999999), Integer.parseInt(add("999", "9999999")));

        assertEquals((123456789 + 987654321), Integer.parseInt(add("123456789", "987654321")));
        assertEquals((987654321 + 123456789), Integer.parseInt(add("987654321", "123456789")));

        assertEquals((98999898 + 989898989), Integer.parseInt(add("98999898", "989898989")));
        assertEquals((989898989 + 98999898), Integer.parseInt(add("989898989", "98999898")));

        assertEquals((999999999 + 111), Integer.parseInt(add("999999999", "111")));
        assertEquals((111 + 999999999), Integer.parseInt(add("111", "999999999")));
    }

    private int compareTo(String a, String b) {
        FastInteger fa = new FastInteger(a);
        FastInteger fb = new FastInteger(b);

        return fa.compareTo(fb);
    }

    private String mult(String a, String b) {
        FastInteger fa = new FastInteger(a);
        fa.mult(FastInteger.valueOf(b));

        return fa.toString();
    }

    private String sub(String a, String b) {
        FastInteger fa = new FastInteger(a);
        fa.sub(FastInteger.valueOf(b));

        return fa.toString();
    }

    private String add(String a, String b) {
        FastInteger fa = new FastInteger(a);
        fa.add(FastInteger.valueOf(b));

        return fa.toString();
    }
}
