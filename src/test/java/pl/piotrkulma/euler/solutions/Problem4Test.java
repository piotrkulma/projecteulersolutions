package pl.piotrkulma.euler.solutions;

import org.junit.Test;
import static org.junit.Assert.assertEquals;


public class Problem4Test {
    @Test
    public void isPalindromeTest() {
        Problem4 problem4 = new Problem4();
        assertEquals(problem4.isPalindrome("PoiuuioP"), true);
        assertEquals(problem4.isPalindrome("9009"), true);

        assertEquals(problem4.isPalindrome(null), false);
        assertEquals(problem4.isPalindrome(""), false);
        assertEquals(problem4.isPalindrome("asdfgdsa"), false);
        assertEquals(problem4.isPalindrome("12345"), false);
        assertEquals(problem4.isPalindrome("Peter"), false);
        assertEquals(problem4.isPalindrome("qwertyoytrewq"), false);
    }
}
