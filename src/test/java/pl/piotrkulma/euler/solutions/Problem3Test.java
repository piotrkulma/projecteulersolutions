package pl.piotrkulma.euler.solutions;

import org.junit.Test;
import static org.junit.Assert.assertArrayEquals;


public class Problem3Test {
    @Test
    public void getPrimeFactorsTest() {
        Problem3 problem3 = new Problem3();
        Long[] values = new Long[4];
        assertArrayEquals(problem3.getPrimeFactors(13195 ).toArray(values), new Long[] {5L, 7L, 13L, 29L});
        assertArrayEquals(problem3.getPrimeFactors(600851475143l).toArray(values), new Long[] {71L, 839L, 1471L, 6857L});
    }
}
