package pl.piotrkulma.euler.solutions;

import org.apache.log4j.Logger;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * https://projecteuler.net/problem=11
 **/

public class Problem11 {
    private final static Logger LOG = Logger.getLogger(Problem11.class);

    private interface I {
        int checkOrientation(int grid[][], int size, int i, int j);
    }

    private I[] checks = new I[]{
            Problem11::checkVertical,
            Problem11::checkHorizontal,
            Problem11::checkDiagonalTLCToBRC,
            Problem11::checkDiagonalTRCToBLC};

    public int searchMaxInGrid(int grid[][], int size) {
        int localMax;
        int globalMax = -1;

        for(int i=0; i<size; i++) {
            for(int j=0; j<size; j++) {

                localMax = -1;
                for(int k =0; k<4; k++) {
                    int val = checks[k].checkOrientation(grid, size, i, j);
                    if(val > localMax) {
                        localMax = val;
                    }
                }

                if(localMax > globalMax) {
                    globalMax = localMax;
                }
            }
        }

        return globalMax;
    }

    public static  int checkHorizontal(int grid[][], int size, int i, int j) {
        int result = 1;
        int maxIndex = size -1;

        if(!(j + 1 <= maxIndex && j + 2 <= maxIndex && j + 3 <= maxIndex)) {
            return  -1;
        }

        for(int k=0; k<4; k++) {
            result *= grid[i][j + k];
        }
        return result;
    }

    public static  int checkVertical(int grid[][], int size, int i, int j) {
        int result = 1;
        int maxIndex = size -1;

        if(!(i + 1 <= maxIndex && i + 2 <= maxIndex && i + 3 <= maxIndex)) {
            return  -1;
        }

        for(int k=0; k<4; k++) {
            result *= grid[i + k][j];
        }
        return result;
    }

    /**
     * Diagonally check from Top Left Corner (i, j) to Bottom Right Corner (i+3, j+3)
     *
     * @param grid
     * @param size
     * @param i
     * @param j
     * @return
     */
    public static  int checkDiagonalTLCToBRC(int grid[][], int size, int i, int j) {
        int result = 1;
        int maxIndex = size -1;

        if(!(i + 1 <= maxIndex && i + 2 <= maxIndex && i + 3 <= maxIndex
                && j + 1 <= maxIndex && j + 2 <= maxIndex && j + 3 <= maxIndex)) {
            return  -1;
        }

        for(int k=0; k<4; k++) {
            result *= grid[i + k][j + k];
        }
        return result;
    }

    /**
     * Diagonally check from Top Right Corner (i, j) to Bottom Left Corner (i-3, j+3)
     *
     * @param grid
     * @param size
     * @param i
     * @param j
     * @return
     */
    public static  int checkDiagonalTRCToBLC(int grid[][], int size, int i, int j) {
        int result = 1;
        int maxIndex = size -1;

        if(!(i - 1 >=0 && i - 2 >=0 && i - 3 >=0
                && j + 1 <= maxIndex && j + 2 <= maxIndex && j + 3 <= maxIndex)) {
            return  -1;
        }

        for(int k=0; k<4; k++) {
            result *= grid[i - k][j + k];
        }
        return result;
    }

    public int[][] getGridFromFile(String fileName, int size) throws Exception {
        List<String> listOfLines = readLinesFileFromResources(fileName);

        int[][] grid = new int[size][size];

        for(int i=0; i<listOfLines.size(); i++) {
            String[] splitted = listOfLines.get(i).split(" ", -1);
            for(int  j=0; j<splitted.length; j++) {
                grid[i][j] = Integer.parseInt(splitted[j]);
            }
        }

        return grid;
    }

    public static void printGridIntoStd(int[][] grid, int size) {
        for(int i=0; i<20; i++) {
            for(int j=0; j<20; j++) {
                if(grid[i][j] < 10) {
                    System.out.print("0" + grid[i][j] + " ");
                } else {
                    System.out.print(grid[i][j] + " ");
                }

            }
            System.out.println();
        }
    }

    private List<String> readLinesFileFromResources(String fileName) throws Exception {
        Path path = Paths.get(Problem8.class.getClassLoader().getResource(fileName).toURI());
        return Files.readAllLines(path);
    }

    public static void main(String[] args) throws Exception {
        Problem11 problem = new Problem11();

        int[][] grid = problem.getGridFromFile("problem11.txt", 20);
        printGridIntoStd(grid, 20);
        int max = problem.searchMaxInGrid(grid, 20);
        LOG.info("MAX: " + max);
    }
}
