package pl.piotrkulma.euler.solutions;

import org.apache.log4j.Logger;

/**
 * https://projecteuler.net/problem=7
 **/
public class Problem7 {
    private final static Logger LOG = Logger.getLogger(Problem7.class);

    public long[] eratosthenesSieve(int range) {
        long[] numberArray = initArray(range);

        for(int i=2; i<=range + 1; i++) {
            for(int j=(i+i); j<=range + 1; j+=i) {
                numberArray[j - 2] = -1;
            }
        }

        return numberArray;
    }

    private long[] initArray(int range) {
        long[] numberArray = new long[range];
        for(int i=2; i<=range + 1; i++) {
            numberArray[i - 2] = i;
        }

        return numberArray;
    }

    public static void main(String... args) {
        Problem7 problem = new Problem7();

        long[] numberArray = problem.eratosthenesSieve(110000);
        int counter = 1;
        for(int i=0; i<numberArray.length; i++) {
            if(numberArray[i] != -1) {
                LOG.info(counter++ + ": " + numberArray[i]);
            }
        }
    }
}
