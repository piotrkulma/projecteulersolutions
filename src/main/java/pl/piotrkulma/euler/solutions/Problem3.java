package pl.piotrkulma.euler.solutions;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * https://projecteuler.net/problem=3
 */
public class Problem3 {
    private final static Logger LOG = Logger.getLogger(Problem3.class);

    private static final long PROBLEM3_CASE = 600851475143l;

    /**
     * Source: https://projecteuler.net/problem=3
     *
     * every number n can at most have one prime factor greater than sqrt(n) . If we,
     * after dividing out some prime factor, calculate the square root of the remaining number we
     * can use that square root as upper limit for factor. If factor exceeds this square root
     * we know the remaining number is prime.
     *
     * @param n
     * @return
     */
    public List<Long> getPrimeFactors(long n) {
        List<Long> factors = new ArrayList<>();
        long i = 2;
        double maxFactor = Math.sqrt(n);
        while(n > 1 && i<=maxFactor) {
            if(n % i == 0) {
                n = n / i;
                factors.add(i);
                maxFactor = Math.sqrt(n);
            } else {
                i++;
            }
        }

        if(n > maxFactor) {
            factors.add(n);
        }

        return factors;
    }

    public static void main(String... args) {
        Problem3 problem3 = new Problem3();
        problem3.getPrimeFactors(PROBLEM3_CASE).stream().forEach(l -> LOG.info("PRIME FACTOR IS: " + l));
    }
}
