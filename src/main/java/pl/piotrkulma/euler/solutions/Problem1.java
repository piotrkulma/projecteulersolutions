package pl.piotrkulma.euler.solutions;

import org.apache.log4j.Logger;

/**
 * https://projecteuler.net/problem=1
 */
public class Problem1 {
    private final static Logger LOG = Logger.getLogger(Problem1.class);

    public int getSolution(int n) {
        int sum = 0;
        for(int i=1; i<n; i++) {
            if(i % 3 == 0 || i % 5 == 0) {
                sum += i;
            }
        }

        return sum;
    }

    public static void main(String... args) {
        Problem1 problem1 = new Problem1();
        LOG.info("PROBLEM_1 SOLUTION IS: " + problem1.getSolution(1000));
    }
}
