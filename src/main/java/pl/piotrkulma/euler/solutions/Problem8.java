package pl.piotrkulma.euler.solutions;

import org.apache.log4j.Logger;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * https://projecteuler.net/problem=8
 **/
public class Problem8 {
    private final static Logger LOG = Logger.getLogger(Problem8.class);

    public String readFileFromResources(String fileName) throws Exception {
        StringBuilder stringBuilder = new StringBuilder();

        Path path = Paths.get(Problem8.class.getClassLoader().getResource(fileName).toURI());
        Files.readAllLines(path).stream().forEach(line -> stringBuilder.append(line));

        return stringBuilder.toString();
    }

    public long getGreatestProduct(String text, int adj) {
        long max = 0, temp;

        for(int i=0; i<text.length() - (adj - 1); i++) {
            String substring = text.substring(i, i + adj);
            temp = productFromText(substring);

            if(temp > max) {
                max = temp;
            }
        }

        return max;
    }

    private long productFromText(String text) {
        long mult = 1;
        for (int i=0; i<text.length(); i++) {
            mult *= text.charAt(i) - 48;
        }

        return mult;
    }

    public static void main(String... args) throws Exception {
        Problem8 problem = new Problem8();
        String text = problem.readFileFromResources("problem8.txt");
        LOG.info(problem.getGreatestProduct(text, 13));
    }
}