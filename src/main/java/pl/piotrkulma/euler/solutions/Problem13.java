package pl.piotrkulma.euler.solutions;

import org.apache.log4j.Logger;
import pl.piotrkulma.euler.solutions.utils.FastInteger;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * https://projecteuler.net/problem=13
 **/
public class Problem13 {
    private final static Logger LOG = Logger.getLogger(Problem13.class);

    public List<String> readLinesFileFromResources(String fileName) throws Exception {
        Path path = Paths.get(Problem8.class.getClassLoader().getResource(fileName).toURI());
        return Files.readAllLines(path);
    }

    public static void main(String[] args) throws Exception {
        Problem13 problem = new Problem13();
        List<String> lines = problem.readLinesFileFromResources("problem13.txt");

        FastInteger fint = new FastInteger();
        lines.stream().forEach(s -> fint.add(FastInteger.valueOf(s)));

        LOG.info("RESULT: " + fint);
    }
}
