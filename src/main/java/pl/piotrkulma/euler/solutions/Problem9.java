package pl.piotrkulma.euler.solutions;

import org.apache.log4j.Logger;

/**
 * https://projecteuler.net/problem=9
 **/
public class Problem9 {
    private final static Logger LOG = Logger.getLogger(Problem9.class);

    public void findNumbers(int n) {
        int T = 1000;

        for(int b=1; b<n; b++) {
            for(int c=1; c<n; c++) {
                int a = T - b - c;
                if(a < b && b < c
                        &&(pow(a) + pow(b)) == pow(c)
                        && (a + b + c) == T) {
                    LOG.info("________________________________________");
                    LOG.info("A: " + a + " B: " + b + " C: " + c);
                    LOG.info("P: " + (a*b*c));
                    //return;
                }
            }
        }
    }

    private int pow(int n) {
        return n*n;
    }

    public static void main(String[] args) {
        Problem9 problem = new Problem9();

        problem.findNumbers(1000);
    }
}
