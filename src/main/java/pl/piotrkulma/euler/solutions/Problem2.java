package pl.piotrkulma.euler.solutions;

import org.apache.log4j.Logger;

/**
 * https://projecteuler.net/problem=2
 */
public class Problem2 {
    /**
     * 31 is 2178309
     * 32 is 3524578
     * 33 is 5702887
     */
    private final static Logger LOG = Logger.getLogger(Problem2.class);

    public long getFibonaccOddSum(int n) {
        long fibNum;
        long sum = 0;
        long prev[] = {1, 2};

        for(int i=1; i<=n; i++) {
            if(i==1) {
                fibNum = prev[0];
            }else if(i==2) {
                fibNum = prev[1];
            } else {
                fibNum = (prev[0] + prev[1]);
                prev[0] = prev[1];
                prev[1] = fibNum;
            }

            if(fibNum % 2 == 0) {
                sum += fibNum;
            }
        }

        return sum;
    }

    public static void main(String... args) {
        Problem2 problem2 = new Problem2();
        LOG.info("PROBLEM_2 SOLUTION IS: " + problem2.getFibonaccOddSum(32));
    }
}
