package pl.piotrkulma.euler.solutions;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * https://projecteuler.net/problem=4
 */
public class Problem4 {
    private final static Logger LOG = Logger.getLogger(Problem4.class);

    public boolean isPalindrome(String s) {
        if(StringUtils.isBlank(s) || s.length() % 2 != 0) {
            return false;
        }

        for(int i=0; i<s.length()/2; i++) {
            if(s.charAt(i) != s.charAt((s.length() - 1) - i)) {
                return false;
            }
        }

        return true;
    }

    public void getThreeDigitNumberPalindromes() {
        int product;
        int biggestPalNumber = 0;

        for(int i=100; i<=999; i++) {
            for (int j=100; j<999; j++) {
                product = i*j;
                if(isPalindrome(Integer.toString(product)) && product > biggestPalNumber) {
                    biggestPalNumber = product;
                    LOG.info(product);
                }
            }
        }
    }

    public static void main(String... args) {
        Problem4 problem = new Problem4();
        problem.getThreeDigitNumberPalindromes();
    }
}
