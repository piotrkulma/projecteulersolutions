package pl.piotrkulma.euler.solutions;

import org.apache.log4j.Logger;

import java.util.*;

/**
 * https://projecteuler.net/problem=12
 **/
public class Problem12 {
    private final static Logger LOG = Logger.getLogger(Problem12.class);

    public long getNumberOfDivisors(long number) {
        long div = 1;

        Collection<Long> exp = findPrimeDividorsExponents(number);
        for(long e : exp) {
            div *= (e + 1);
        }

        return div;
    }

    private Collection<Long> findPrimeDividorsExponents(long n) {
        long factor = 2;
        double maxFactor = Math.sqrt(n);

        Map<Long, Long> factors = new HashMap<>();

        while (n > 1) {
            if (n % factor == 0) {
                n = n / factor;
                maxFactor = Math.sqrt(n);
                addFactor(factors, factor);
            } else {
                factor++;
            }
        }

        if (n > maxFactor) {
            addFactor(factors, n);
        }


        return factors.values();
    }

    private void addFactor(Map<Long, Long> factors, long factor) {
        long value = 0;
        if(factors.containsKey(factor)) {
            value = factors.get(factor);
        }

        value++;
        factors.put(factor, value);
    }

    public long getSumOfNaturalNumbers(long n) {
        return n * (n-1) / 2;
    }

    public static void main(String[] args) throws Exception {
        int c = 9000;
        long max = 0;
        long num, div = 0;

        Problem12 problem12 = new Problem12();

        while(div < 500) {
            num = problem12.getSumOfNaturalNumbers(c);
            div = problem12.getNumberOfDivisors(num);

            if(div > max) {
                max = div;
            }

            LOG.info("ITER: " + c + " NUM: " + num + " DIVISORS: " + div + " CURRENT MAX: " + max);
            c++;
        }
    }
 }
