package pl.piotrkulma.euler.solutions;

import org.apache.log4j.Logger;
import pl.piotrkulma.euler.solutions.utils.ArrayUtils;
import pl.piotrkulma.euler.solutions.utils.FastInteger;
import pl.piotrkulma.euler.solutions.utils.FastIntegerExt;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * https://projecteuler.net/problem=15
 **/
public class Problem15 {
    private final static Logger LOG = Logger.getLogger(Problem15.class);

    private List<String> history;
    private Stack<Integer> stack;

    public Problem15() {
        history = new ArrayList<>();
        stack = new Stack<>();
    }

    public long findAllPaths(int n) {
        int[][] graph = createGraphNotePathTable(n + 1);
        traverseGraph(graph);

        StringBuilder stringBuilder = new StringBuilder();
        appendZeorsIfNeeded(stringBuilder, ((n + 1) * (n + 1)) - 1);
        String endOfPath = stringBuilder.toString();
        return history.stream().filter(h -> h.endsWith(endOfPath)).count();
    }

    private void traverseGraph(int[][] graph) {
        int newPath;
        int row = 0;

        while(true) {
            stack.push(row);

            saveStateInHistory();

            newPath = findPath(graph, row, graph[0].length);

            if(newPath != -1) {
                row = newPath;
            } else if(stack.size() > 1){
                stack.pop();
                row = stack.pop();
            } else if(stack.size() == 1) {
                break;
            }
        }
    }

    private int findPath(int[][] graph, int row, int n) {
        for(int i=0; i<n; i++) {
            if(graph[row][i] == 1 && !isStateInHistory(getState(i))) {
                return i;
            }
        }

        return -1;
    }

    private boolean isStateInHistory(String state) {
        return history.stream().anyMatch(h -> h.equals(state));
    }

    private void saveStateInHistory() {
        history.add(getState());
    }

    private String getState(Integer newValue) {
        StringBuilder stringBuilder = new StringBuilder(getState());
        appendZeorsIfNeeded(stringBuilder, newValue);

        return stringBuilder.toString();
    }

    private String getState() {
        StringBuilder stringBuilder = new StringBuilder();

        stack.stream()
                .forEach( i -> appendZeorsIfNeeded(stringBuilder, i));

        return stringBuilder.toString();
    }

    private void appendZeorsIfNeeded(StringBuilder stringBuilder, int value) {
        if(value < 10) {
            stringBuilder.append(0);
            stringBuilder.append(0);
        }else if(value > 10 && value < 100) {
            stringBuilder.append(0);
        }

        stringBuilder.append(value);
    }

    private int[][] createGraphNotePathTable(int n) {
        int[][] nt = createNodeTable(n);
        return createNodePathArray(nt, n);
    }

    private int[][] createNodePathArray(int[][] nodeTable, int n) {
        int size = n*n;
        int[][] nodePath = new int[size][size];

        int actualCell, cellFromRight, cellFromDown;

        for(int i=0; i<n; i++) {
            for(int j=0; j<n; j++) {
                actualCell = nodeTable[i][j];
                if(ArrayUtils.isCellInArray(n, i, j + 1)) {
                    cellFromRight = nodeTable[i][j + 1];
                    nodePath[actualCell][cellFromRight] = 1;
                }

                if(ArrayUtils.isCellInArray(n, i + 1, j)) {
                    cellFromDown = nodeTable[i + 1][j];
                    nodePath[actualCell][cellFromDown] = 1;
                }
            }
        }

        return nodePath;
    }

    private int[][] createNodeTable(int n) {
        int g[][] = new int[n][n];

        int counter = 0;
        for(int i=0; i<n; i++) {
            int j = 0;
            while(ArrayUtils.isCellInArray(n, i - j, j)) {
                g[i - j][j] = counter++;
                j++;
            }
        }

        counter = (n*n) - 1;
        int ti, tj;
        for(int i=(n - 1); i>=0; i--) {
            ti = i;
            tj = n - 1;
            while (ArrayUtils.isCellInArray(n, ti, tj)) {
                g[ti][tj] = counter--;
                ti++;
                tj--;
            }
        }

        return g;
    }

    public static void main(String[] args) {
        //Problem15 problem = new Problem15();
        //result = problem.findAllPaths(n);

        //LOG.info(MathUtils.factorial(new FastInteger("21")));
    }
}