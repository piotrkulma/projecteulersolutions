package pl.piotrkulma.euler.solutions;

import org.apache.log4j.Logger;

/**
 * https://projecteuler.net/problem=5
 */
public class Problem5 {
    private final static Logger LOG = Logger.getLogger(Problem5.class);

    public boolean isEvenlyDivisable(long number, int rangeEnd) {
        for(int i=1; i<=rangeEnd; i++) {
            if(number %  i != 0) {
                return false;
            }
        }

        return true;
    }

    public long getSmallestNumberDivisableByRange(int rangeEnd) {
        long number = 21;
        while(true) {
            if(isEvenlyDivisable(number, 20)) {
                return number;
            }
            number++;
        }
    }

    public static void main(String... args) {
        Problem5 problem = new Problem5();
        LOG.info("SMALLEST NUMBER: " + problem.getSmallestNumberDivisableByRange(20));
    }
}
