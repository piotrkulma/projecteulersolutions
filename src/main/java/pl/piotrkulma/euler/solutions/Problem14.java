package pl.piotrkulma.euler.solutions;

import org.apache.log4j.Logger;

/**
 * https://projecteuler.net/problem=14
 **/
public class Problem14 {
    private final static Logger LOG = Logger.getLogger(Problem14.class);

    public long countCollatzSequenceElements(long n) {
        long elements = 1;
        while(n > 1) {
            if (n % 2 == 0) {
                n = n / 2;
            } else {
                n = 3 * n + 1;
            }

            elements++;
        }

        return elements;
    }

    public static void main(String[] args) {
        Problem14 problem = new Problem14();

        long maxChain = -1;
        long maxStartNum = 0;

        for(long n = 0; n<1000000; n++) {
            long el = problem.countCollatzSequenceElements(n);
            if(el > maxChain) {
                maxChain = el;
                maxStartNum = n;
            }
        }

        LOG.info("MAX START NUM: " + maxStartNum + " MAX CHAIN: " + maxChain);
    }
}
