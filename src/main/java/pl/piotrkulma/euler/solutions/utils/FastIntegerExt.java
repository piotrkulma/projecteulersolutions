package pl.piotrkulma.euler.solutions.utils;

public final class FastIntegerExt {
    public static final String MINUS_SIGN = "-";
    public static final String NUMBER_REGEX = "[+-]*[0-9]+";

    public String add(String var1, String var2) {
        if(var2.length() > var1.length()) {
            return add(var2, var1);
        }

        int mem = 0;
        int num1, num2, res;

        StringBuilder result = new StringBuilder();

        for(int i=0; i<var1.length(); i++) {
            num1 = var1.charAt((var1.length() - 1) - i) - 48;
            num2 = 0;

            if((var2.length() - 1) - i >= 0) {
                num2 = var2.charAt((var2.length() - 1) - i) - 48;
            }

            res = (num1 + num2) + mem;

            if(res >= 10) {
                res = res % 10;
                mem = 1;
            } else {
                mem = 0;
            }

            result.append(res);
        }

        if(mem > 0) {
            result.append(mem);
        }

        return result.reverse().toString();
    }

    public String sub(String var1, String var2) {
        return sub(var1, var2, false);
    }

    private String sub(String var1, String var2, boolean resultIsMinus) {
        if(isNotMinus(var1) && isMinus(var2)) {
            return add(var1, removeMinusSign(var2));
        } else if(isMinus(var1) && isMinus(var2)) {
            return sub(removeMinusSign(var2), removeMinusSign(var1));
        } else if(isMinus(var1) && isNotMinus(var2)) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(MINUS_SIGN);
            stringBuilder.append(add(removeMinusSign(var1), var2));
            return stringBuilder.toString();
        }

        if(isGreater(var2, var1)) {
            return sub(var2, var1, true);
        }

        int mem = 0;
        int num1, num2, res;

        StringBuilder result = new StringBuilder();

        for(int i=0; i<var1.length(); i++) {
            num1 = var1.charAt((var1.length() - 1) - i) - 48;
            num2 = 0;

            if((var2.length() - 1) - i >= 0) {
                num2 = var2.charAt((var2.length() - 1) - i) - 48;
            }

            num1 -= mem;

            if(num1 < num2) {
                num1 += 10;
                mem = 1;
            } else {
                mem = 0;
            }

            res = num1 - num2;

            result.append(res);
        }

        if(resultIsMinus) {
            result.append(MINUS_SIGN);
        }

        return result.reverse().toString();
    }

    /**
     * Very inefficient way of multiplying. Algorithm is done by adding, eg.
     * 3x4 = 4 + 4 + 4
     * To refactor: Implementing simple school algorithm of multiplification
     */
    //TODO improve algorithm
    public String mult(String var1, String var2) {
        String res = "0";
        for(int i=0; i<Integer.parseInt(var1); i++) {
            res = add(res, var2);
        }

        return res;
    }

    //TODO all logic
    public String div(String var1, String var2) {
        return null;
    }

    /**
     * Validte number candidate against null value and having non numeric characters
     *
     * @param candidate
     */
    public void validate(String candidate) {
        if(candidate == null) {
            throw new RuntimeException("String cannot be null");
        }

        if(!candidate.matches(NUMBER_REGEX)) {
            throw new RuntimeException("String '" + candidate + "' cannot be converted into numeric.");
        }
    }

/*    public void validate(String candidateOne, String candidateTwo) {
        validate(candidateOne);
        validate(candidateTwo);
    }*/

    /**
     * Removes leading zeros and '+' character
     *
     * @param number
     * @return
     */
    //TODO all logic
    public String normalize(String number) {
        return number;
    }

    public String removeMinusSign(String number) {
        return number.replaceAll(MINUS_SIGN, "");
    }

    /**
     * Returns true when a is greater than b. Otherwise returns false
     *
     * @param a
     * @param b
     * @return
     */
    public boolean isGreater(String a, String b) {
        if(a.length() < b.length() || a.equals(b)) {
            return false;
        } else if(a.length() > b.length()) {
            return true;
        }

        int ai, bi;

        for(int i = 0; i<a.length(); i++) {
            ai = a.charAt(i) - 48;
            bi = b.charAt(i) - 48;

            if(ai != bi) {
                if(ai>bi) {
                    return true;
                }

                return false;
            }
        }

        return true;
    }

    private boolean isMinus(String s) {
        return s.startsWith(MINUS_SIGN);
    }

    private boolean isNotMinus(String s) {
        return !isMinus(s);
    }
}
