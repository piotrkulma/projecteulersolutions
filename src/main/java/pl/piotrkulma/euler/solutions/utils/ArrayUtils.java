package pl.piotrkulma.euler.solutions.utils;

public final class ArrayUtils {
    private ArrayUtils() {
    }

    public static void printArray(int[][] g, int n) {
        for(int i=0; i<n; i++) {
            for(int j=0; j<n; j++) {
                System.out.print(g[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static  boolean isCellInArray(int size, int i, int j) {
        return i >= 0 && i<size && j>=0 && j < size;
    }
}
