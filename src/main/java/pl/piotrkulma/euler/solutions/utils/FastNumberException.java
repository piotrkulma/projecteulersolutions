package pl.piotrkulma.euler.solutions.utils;

public class FastNumberException extends RuntimeException {
    public FastNumberException(String message) {
        super(message);
    }
}
