package pl.piotrkulma.euler.solutions.utils;

public final class MathUtils {
    public static FastInteger factorial(FastInteger n) {
        FastInteger one = new FastInteger(FastInteger.STR_ONE);

        FastInteger result = new FastInteger(FastInteger.STR_ONE);

        FastInteger i = new FastInteger(FastInteger.STR_ONE);

        while(!i.equals(n)) {
            i.add(one);
            result.mult(i);
        }

        return result;
    }
}
