package pl.piotrkulma.euler.solutions.utils;

import java.util.Objects;

public final class FastInteger implements Comparable<FastInteger>{
    public static final String STR_ZERO = "0";
    public static final String STR_ONE  = "1";

    private String value;
    private FastIntegerExt ext;

    public FastInteger() {
        ext = new FastIntegerExt();
        setValueOfString(STR_ZERO);
    }

    public FastInteger(String s) {
        ext = new FastIntegerExt();
        setValueOfString(s);
    }

    public FastInteger(FastInteger fi) {
        ext = new FastIntegerExt();
        setValueOfString(fi.value);
    }

    public static FastInteger valueOf(String value) {
        return new FastInteger(value);
    }

    public static FastInteger valueOf(int value) {
        FastInteger fastInteger = new FastInteger();
        fastInteger.setValueOfInt(value);

        return fastInteger;
    }

    @Override
    public String toString() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }

        if(o == null) {
            return false;
        }

        if(getClass() != o.getClass()) {
            return false;
        }

        return equals((FastInteger) o);
    }

    @Override
    public int compareTo(FastInteger o) {
        if(this.equals(o)) {
            return 0;
        }else if(ext.isGreater(this.value, o.value)) {
            return 1;
        }

        return -1;
    }

    public void add(FastInteger fi) {
        add(fi.value);
    }

    public void sub(FastInteger fi) {
        sub(fi.value);
    }

    public void mult(FastInteger fi) {
        mult(fi.value);
    }

    public void div(FastInteger fi) {
        div(fi.value);
    }

    public void setValueOfString(String value) {
        ext.validate(value);
        this.value = ext.normalize(value);
    }

    public void setValueOfInt(int value) {
        this.value = Integer.toString(value);
    }

    private void add(String s) {
        ext.validate(s);
        value = ext.add(value, ext.normalize(s));
    }

    private void sub(String s) {
        ext.validate(s);
        value = ext.sub(value, ext.normalize(s));
    }

    private void mult(String s) {
        ext.validate(s);
        value = ext.mult(s, ext.normalize(value));
    }

    private void div(String s) {
        ext.validate(s);
        value = ext.div(value, ext.normalize(s));
    }

    private boolean equals(FastInteger fi) {
        return Objects.equals(value, fi.value);
    }
}
