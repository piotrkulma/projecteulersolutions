package pl.piotrkulma.euler.solutions;

import org.apache.log4j.Logger;

/**
 * https://projecteuler.net/problem=10
 **/
public class Problem10 {
    private final static Logger LOG = Logger.getLogger(Problem10.class);

    public long[] eratosthenesSieve(int range) {
        long[] numberArray = initArray(range);

        for(int i=2; i<=range + 1; i++) {
            for(int j=(i+i); j<=range + 1; j+=i) {
                numberArray[j - 2] = -1;
            }
        }

        return numberArray;
    }

    private long[] initArray(int range) {
        long[] numberArray = new long[range];
        for(int i=2; i<=range + 1; i++) {
            numberArray[i - 2] = i;
        }

        return numberArray;
    }

    public static void main(String[] args) {
        Problem10 problem = new Problem10();
        long sum = 0;
        long[] es = problem.eratosthenesSieve(2000000-2);

        for(long l : es) {
            if(l > 0) {
                sum += l;
            }
        }

        LOG.info("SUM: " + sum);
    }
}
