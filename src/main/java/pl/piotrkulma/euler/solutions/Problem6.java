package pl.piotrkulma.euler.solutions;

import org.apache.log4j.Logger;

/**
 * https://projecteuler.net/problem=6
 *
 * https://math.stackexchange.com/questions/439220/what-is-the-difference-between-square-of-sum-and-sum-of-square
 */
public class Problem6 {
    private final static Logger LOG = Logger.getLogger(Problem6.class);

    public long getS1(int number) {
        long result = 0;
        for(int i=1; i<=number; i++) {
            for(int j=1; j<=number; j++) {
                result += ((i-j) * (i-j));
            }
        }

        if(result % 2 != 0) {
            throw new RuntimeException("Number must be divisable by two!");
        }
        return result / 2;
    }


    public long getSquareOfSums(int number) {
        long result = 0;
        for(int i=1; i<=number; i++) {
            result += i;
        }

        return result * result;
    }

    public static void main(String... args) {
        Problem6 problem = new Problem6();

        int number = 100;
        long s1 = problem.getS1(number);
        long squareOfSums = problem.getSquareOfSums(number);
        long sumOfSquares = (s1 + squareOfSums) / number;
        LOG.info("RESULT IS: " + s1);
        LOG.info("RESULT IS: " + squareOfSums);
        LOG.info("RESULT IS: " + sumOfSquares);
        LOG.info("FINAL IS: " + (squareOfSums - sumOfSquares));
    }
}
